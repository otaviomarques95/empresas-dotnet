--
-- Banco de dados: `BDempresa`
--
CREATE DATABASE IF NOT EXISTS `BDempresa` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `BDempresa`;

-- --------------------------------------------------------

--
-- Criação da tabela 'EnterpriseTypes'
--

CREATE TABLE `EnterpriseTypes` (
  `ID` int(11) NOT NULL,
  `EnterpriseTypeName` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Criação da tabela 'Enterprises'
--

CREATE TABLE `Enterprises` (
  `ID` int(11) NOT NULL,
  `EmailEnterprise` text,
  `Phone` text,
  `OwnEnterprise` int(11) NOT NULL,
  `EnterpriseName` text,
  `Photo` text,
  `Description` text,
  `City` text,
  `Country` text,
  `Value` double NOT NULL,
  `SharePrice` double NOT NULL,
  `EnterpriseTypeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Criação da tabela 'UserSessions'
--

CREATE TABLE `UserSessions` (
  `ID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `AccessToken` varchar(767) DEFAULT NULL,
  `Client` text,
  `ExpireDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Criação da tabela 'Users'
--

CREATE TABLE `Users` (
  `ID` int(11) NOT NULL,
  `Email` varchar(767) DEFAULT NULL,
  `Password` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Criação da tabela '__EFMigrationsHistory'
--

CREATE TABLE `__EFMigrationsHistory` (
  `MigrationId` varchar(150) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Inserindo dados na tabela 'EnterpriseTypes'
--

INSERT INTO `EnterpriseTypes` (`ID`, `EnterpriseTypeName`) VALUES
(1, 'Empresa Agrônoma'),
(2, 'Aviacao'),
(3, 'Biotech'),
(4, 'Ecologica'),
(5, 'Ecommerce'),
(6, 'Educação'),
(7, 'Fashion'),
(8, 'Fintech'),
(9, 'Alimento'),
(10, 'Games'),
(11, 'Health'),
(12, 'IOT'),
(13, 'Logistics'),
(14, 'Media'),
(15, 'Mining'),
(16, 'Produt'),
(17, 'Real Estate'),
(18, 'Servico'),
(19, 'Smart City'),
(20, 'Social'),
(21, 'Software'),
(22, 'Tecnologia'),
(23, 'Tourismo'),
(24, 'Transporte');

--
-- Inserindo dados na tabela 'Enterprises'
--

INSERT INTO `Enterprises` (`ID`, `EmailEnterprise`, `Facebook`, `Twitter`, `Linkedin`, `Phone`, `OwnEnterprise`, `EnterpriseName`, `Photo`, `Description`, `City`, `Country`, `Value`, `SharePrice`, `EnterpriseTypeID`) VALUES
(1, '','', 0, 'AllRide', '/uploads/enterprise/photo/1/wood_trees_gloomy_fog_haze_darkness_50175_1920x1080.jpg', 'Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city.  Urbanatika, Agro-Urban Industry', 'Santiago', 'Chile', 0, 5000, 21),
(2, '','', 0, 'Alpaca Samka SpA', '/uploads/enterprise/photo/2/WhatsApp_Image_2017-10-31_at_13.47.22.jpeg', 'Alpaca Samka uses alpaca fibres for our “Slow Fashion Project” in association with the Aymaras of the Chilean Andes, producing sustainable luxury accessories and garments using traditional Andean methods and British weaving patterns. We are part of the Inward Investment Program and have been recognised by international organisations. ', 'Viña del Mar', 'Chile', 0, 5000, 7),
(3, '','', 0, 'AnewLytics SpA', '/uploads/enterprise/photo/3/thumb_8016a7d8a952351f3cb4d5f485f43ea1.octet-stream', ' We have one passion: to create value for our customers by analyzing the conversations their customers have with the Contact Center in order to extract valuable and timely information to understand and meet their needs. That´s how AnewLytics was born: a cloud-based analytics service platform that performs 100% automated analysis.', 'Santiago', 'Chile', 0, 5000, 18),
(4, NULL, NULL, 0, 'AQM S.A.', NULL, 'Cold Killer was discovered by chance in the ´90 s and developed by Mrs. Inés Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) up to 30% therefore help plants resists cold weather. ', 'Maule', 'Chile', 0, 5000, 1),
(5, NULL, NULL, 0, 'Árbol Sabores', NULL, 'We are Arbol Sabores, a new generation of healthy food that has a positive impact in the environment and society. We want to change the world through the feeding behaviors of the society, giving seeds of urban orchards in ours products and by innovating with biodegradable packing.', 'Santiago', 'Chile', 0, 5000, 11),
(6, NULL, NULL, 0, 'ArchDaily', NULL, 'Our mission is to improve the quality of life of the next 3 billion people living in cities by 2050, providing inspiration, knowledge, and tools to the architects who fill face this challenge.', 'Santiago', 'Chile', 0, 5000, 14),
(7, NULL, NULL, 0, 'Aveeza', NULL, 'Aveeza is an intelligent platform specially developed for managing school transportation, anywhere. From real time ridership status, route optimisation, fleet maintenance to driver management, Aveeza brings world class logistics to school transportation, so the world’s most precious cargo, children, can be monitored in full transparency that the industry finally deserves.', 'Providencia', 'Chile', 0, 5000, 24),
(8, NULL, NULL, 0, 'BREAL ESTATE SPA', NULL, 'Breal Estate is a Chilean company established in 2013, which, in a strategic alliance with Salesforce.com, initially developed an application for property management. Currently, BReal is an application that incorporates all the functions necessary to manage different processes of the real estate business: property management , sales, leases, common expenses and projects with the best practices in mind. It is delivered as a service (SaaS) and accessed via Internet from any device.', 'SANTIAGO', 'Chile', 0, 5000, 17),
(9, '','', 0, 'Capitalizarme.com', '/uploads/enterprise/photo/9/cruzeiro.png', 'We are a intermediary between developers in real estate and small investors who want to get good investment opportunities.  ', 'Santiago', 'Chile', 0, 5000, 17),
(10,NULL, NULL, 0, 'Capta Hydro', NULL, 'Capta Hydro is a clean energy technology startup that develops and commercialises hydropower technology for generation in artificial canals without the need of falls, that are economically competitive to other distributed generation alternatives and electric utility tariffs. Our BHAG is to install 1 GW of our technology  by 2027.', 'Santiago', 'Chile', 0, 5000, 4),


--
-- Inserindo dados na tabela 'UserSessions'
--

INSERT INTO `UserSessions` (`ID`, `UserID`, `AccessToken`, `Client`, `ExpireDate`) VALUES
(1, 1, '6UA0vgDGW0ub2Z/i4l0jRw==', 'jEx0DUPGdEm5+HEsbirJ4Q==', '2019-02-11 20:50:01'),
(2, 1, '6sI67zIb1kKDbWGztdnK0Q==', 'oqw2npRLa0uCJZ0F0gShnw==', '2019-02-11 21:49:39'),
(3, 1, 'XwDPWs6W0kSv2KDPtILLGw==', 'MlDYYIflGkyHWqJTpNZKvw==', '2019-02-11 21:50:22');

--
-- Inserindo dados na tabela 'Users'
--

INSERT INTO `Users` (`ID`, `Email`, `Password`) VALUES
(1, 'testeapple@ioasys.com.br', '7QA6z4dimFeNTM26pQaP56p0Fy0=');

--
-- Inserindo dados para tabela '__EFMigrationsHistory'
--

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`) VALUES
('20190128192136_InitialCreate', '2.1.2-rtm-30932'),
('20190128202911_Authentication', '2.1.2-rtm-30932');

--
-- Alterando índice da tabela 'EnterpriseTypes'
--
ALTER TABLE `EnterpriseTypes`
  ADD PRIMARY KEY (`ID`);

--
-- Alterando índice da tabela 'Enterprises'
--
ALTER TABLE `Enterprises`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `IX_Enterprises_EnterpriseTypeID` (`EnterpriseTypeID`);

--
-- Alterando índice da tabela 'UserSessions'
--
ALTER TABLE `UserSessions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `UserSession_AccessToken_Index` (`AccessToken`),
  ADD KEY `IX_UserSessions_UserID` (`UserID`);

--
-- Alterando índice da tabela 'Users'
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `User_Email_Index` (`Email`);

--
-- Alterando índice da tabela '__EFMigrationsHistory'
--
ALTER TABLE `__EFMigrationsHistory`
  ADD PRIMARY KEY (`MigrationId`);

--
-- Alterando para realizar o AUTO_INCREMENT na tabela 'EnterpriseTypes'
--
ALTER TABLE `EnterpriseTypes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- Alterando para realizar o AUTO_INCREMENT na tabela 'Enterprises'
--
ALTER TABLE `Enterprises`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- Alterando para realizar o AUTO_INCREMENT na tabela 'UserSessions'
--
ALTER TABLE `UserSessions`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Alterando para realizar o AUTO_INCREMENT na tabela 'Users'
--
ALTER TABLE `Users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Chave estrangeira na tabelas 'Enterprises'
--
ALTER TABLE `Enterprises`
  ADD CONSTRAINT `FK_Enterprises_EnterpriseTypes_EnterpriseTypeID` FOREIGN KEY (`EnterpriseTypeID`) REFERENCES `EnterpriseTypes` (`ID`) ON DELETE CASCADE;

--
-- Chave estrangeira na tabelas 'UserSessions'
--
ALTER TABLE `UserSessions`
  ADD CONSTRAINT `FK_UserSessions_Users_UserID` FOREIGN KEY (`UserID`) REFERENCES `Users` (`ID`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
