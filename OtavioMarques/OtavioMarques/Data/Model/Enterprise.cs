﻿using OtavioMarques.Data.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace OtavioMarques.Data.Model
{
    public class Enterprise
    {
        public int ID { get; set; }

        public string EmailEnterprise { get; set; }

        public string Phone { get; set; }

        public bool OwnEnterprise { get; set; }

        public string EnterpriseName { get; set; }

        public string Photo { get; set; }

        public string Description { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public double Value { get; set; }

        public double SharePrice { get; set; }

        [JsonIgnore]
        public int EnterpriseTypeID { get; set; }

        [ForeignKey(nameof(EnterpriseTypeID))]
        public EnterpriseType EnterpriseType { get; set; }

    }
}
