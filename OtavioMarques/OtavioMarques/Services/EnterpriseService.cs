﻿using Microsoft.EntityFrameworkCore;
using OtavioMarques.Data;
using OtavioMarques.Data.Model;
using OtavioMarques.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OtavioMarques.Services
{
    public class EnterpriseService : IEnterpriseService
    {
        readonly EnterpriseContext dataContext;

        public EnterpriseService(EnterpriseContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public Enterprise Get(int id)
        {
            return this.dataContext.Enterprises
                .Include(p => p.EnterpriseType)
                .FirstOrDefault(p => p.ID == id);
        }

        public IEnumerable<Enterprise> GetAllByTypeAndName(int? enterpriseTypeID, string name)
        {
            IQueryable<Enterprise> query = this.dataContext.Enterprises
                .Include(p => p.EnterpriseType)
                .OrderBy(p => p.ID);

            if (enterpriseTypeID.HasValue)
            {
                query = query.Where(p => p.EnterpriseTypeID == enterpriseTypeID);
            }

            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(p => p.EnterpriseName.Contains(name));
            }

            return query.ToArray();
        }
    }
}
