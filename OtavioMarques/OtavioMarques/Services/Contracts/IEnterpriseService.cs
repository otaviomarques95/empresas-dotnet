﻿using OtavioMarques.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OtavioMarques.Services.Contracts
{
    public interface IEnterpriseService
    {
        Enterprise Get(int id);
        IEnumerable<Enterprise> GetAllByTypeAndName(int? enterpriseTypeID, string name);
    }
}
